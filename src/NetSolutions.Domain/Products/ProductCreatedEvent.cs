﻿using System;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.Domain.Products
{
    public sealed class ProductCreatedEvent : AggregateCreatedEvent<Guid>, IProductEvent
    {
        public string Name { get; }
        
        public string Description { get; }
        
        public double Price { get; }

        public ProductCreatedEvent(Guid aggregateId, DateTime timestamp, string name, string description, double price)
            : base(aggregateId, timestamp)
        {
            Name = name;
            Description = description;
            Price = price;
        }
        
        public override string ToString() => $"{Name} ({Description} - Price:${Price}).";
    }
}