﻿using System;
using System.Collections.Generic;
using NetSolutions.EventSourcing.Abstractions.Domain;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.Domain.Products
{
    public class Product : Aggregate<Guid>
    {
        public Product(string name, string description, double price)
            : base(new ProductCreatedEvent(Guid.NewGuid(), DateTime.UtcNow, name, description, price))
        { }

        public Product(Guid id, IEnumerable<IAggregateEvent<Guid>> savedEvents)
            : base(id, savedEvents)
        { }

        public Product(Guid id, IAggregateSnapshot<Guid> snapshot, IEnumerable<IAggregateEvent<Guid>> savedEvents)
            : base(id, snapshot, savedEvents)
        { }

        public string Name { get; private set; }
        
        public string Description { get; private set; }

        public double Price { get; private set; }

        public StatusProduct Status { get; set; }

        public void Delete() => ReceiveEvent(new ProductDeletedEvent(Id, GetNextVersion(), DateTime.UtcNow));

        protected override void ApplyEvent(IAggregateEvent<Guid> @event)
        {
            if (!(@event is IProductEvent))
            {
                throw new AggregateException($"This event does not correspond to {nameof(Product)}.");
            }
            
            switch (@event)
            {
                case ProductCreatedEvent created:
                    Name = created.Name;
                    Description = created.Description;
                    Price = created.Price;
                    Status = StatusProduct.Enabled;
                    break;
                case ProductUpdatedEvent updated:
                    Name = updated.Name;
                    Description = updated.Description;
                    break;
                case ProductDeletedEvent _:
                    Status = StatusProduct.Deleted;
                    break;
            }
        }
        
        public void Update(string name, string description) => ReceiveEvent(new ProductUpdatedEvent(Id, GetNextVersion(), DateTime.Now, name, description));

        public void UpdatePrice(double price) => ReceiveEvent(new ProductPriceChangedEvent(Id, GetNextVersion(), DateTime.Now, price));
    }
}
