﻿using System;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.Domain.Products
{
    public sealed class ProductDeletedEvent : AggregateEvent<Guid>, IProductEvent
    {
        public ProductDeletedEvent(Guid aggregateId, int aggregateVersion, DateTime timestamp) 
            : base(aggregateId, aggregateVersion, timestamp)
        {
        }
    }
}