﻿using Ardalis.GuardClauses;

namespace NetSolutions.Domain.Products
{
    public class ProductEntity
    {
        private string _description;
        private string _name;
        private int _price;

        protected ProductEntity() { }

        public ProductEntity(string name, string description, int price)
        {
            SetName(name);
            SetDescription(description);
            SetPrice(price);
        }

        public string GetName() => _name;

        public void SetName(string name)
        {
            Guard.Against.NullOrEmpty(name, nameof(name));
            _name = name;
        }

        public int GetPrice() => _price;

        public void SetPrice(int price)
        {
            Guard.Against.Zero(price, nameof(price));
            _price = price;
        }

        public string GetDescription() => _description;

        public void SetDescription(string description)
        {
            Guard.Against.NullOrEmpty(description, nameof(description));
            _description = description;
        }
    }
}
