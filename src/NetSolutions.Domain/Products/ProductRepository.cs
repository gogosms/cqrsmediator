﻿using System;
using System.Threading.Tasks;
using NetSolutions.EventSourcing.Abstractions.Persistence;
using NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence;

namespace NetSolutions.Domain.Products
{
    public class ProductRepository : AggregateRepository<Product, Guid>, IProductRepository
    {
        public ProductRepository(
            IEventStore<Product, Guid> eventStore,
            ISnapshotStore<Product, Guid> snapshotStore)
            : base(eventStore, snapshotStore)
        { }

        public Task Save(Product product) => SaveAggregateAsync(product);

        public Task<Product> Get(Guid id,
            bool ignoreSnapshot = false,
            int version = -1) =>
            FindAggregateAsync(id, ignoreSnapshot, version);

        public Task<Guid[]> GetIds() => GetAggregateIdsAsync();
    }
}