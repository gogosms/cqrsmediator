﻿using System;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.Domain.Products
{
    public sealed class ProductPriceChangedEvent : AggregateEvent<Guid>, IProductEvent
    {
        public double Prince { get; }

        public ProductPriceChangedEvent(Guid aggregateId, 
            int aggregateVersion, DateTime timestamp, double prince) : base(aggregateId, aggregateVersion, timestamp)
        {
            Prince = prince;
        }
    }
}