﻿using System;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.Domain.Products
{
    public sealed class ProductUpdatedEvent : AggregateEvent<Guid>, IProductEvent
    {
        public string Name { get; }
        
        public string Description { get; }

        public ProductUpdatedEvent(Guid aggregateId, int aggregateVersion, DateTime timestamp, string name, string description) 
            : base(aggregateId, aggregateVersion, timestamp)
        {
            Name = name;
            Description = description;
        }
    }
}