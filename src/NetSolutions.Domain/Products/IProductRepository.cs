﻿using System;
using System.Threading.Tasks;

namespace NetSolutions.Domain.Products
{
    public interface IProductRepository
    {
        Task Save(Product product);
        
        Task<Product> Get(Guid id,
            bool ignoreSnapshot = false,
            int version = -1);
        
        Task<Guid[]> GetIds();
    }
}