﻿using System;
using MediatR;

namespace NetSolutions.Mvc.Commands
{
    public class UpdatePriceProductCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public double Price { get; set; }
        
        public UpdatePriceProductCommand(Guid id, double price)
        {
            Id = id;
            Price = price;
            
        }
    }
}