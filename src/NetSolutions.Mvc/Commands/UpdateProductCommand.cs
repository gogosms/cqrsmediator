﻿using System;
using MediatR;

namespace NetSolutions.Mvc.Commands
{
    public class UpdateProductCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }
        
        public UpdateProductCommand(Guid id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
            
        }
    }
}