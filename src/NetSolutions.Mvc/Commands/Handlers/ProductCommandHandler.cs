﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NetSolutions.Domain.Products;

namespace NetSolutions.Mvc.Commands.Handlers
{
    public class ProductCommandHandler : 
        IRequestHandler<CreateProductCommand, Guid>, 
        IRequestHandler<DeleteProductCommand, Guid>,
        IRequestHandler<UpdateProductCommand, Guid>,
        IRequestHandler<UpdatePriceProductCommand, Guid>
    {
        private readonly IProductRepository _repository;
        
        public ProductCommandHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<Guid> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var product = new Product(request.Name, request.Description, request.Price);
            await _repository.Save(product);
            return product.Id;
        }

        public async Task<Guid> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            var product = await _repository.Get(request.Id);
            if (product is null)
            {
                throw new AggregateException($"Does not found Product: {request.Id}.");
            }
            product.Delete();
            await _repository.Save(product);
            return product.Id;
        }

        public async Task<Guid> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var product = await _repository.Get(request.Id);
            if (product is null)
            {
                throw new AggregateException($"Does not found Product: {request.Id}.");
            }
            
            product.Update(request.Name, request.Description);
            await _repository.Save(product);
            return product.Id;
        }

        public async Task<Guid> Handle(UpdatePriceProductCommand request, CancellationToken cancellationToken)
        {
            var product = await _repository.Get(request.Id);
            if (product is null)
            {
                throw new AggregateException($"Does not found Product: {request.Id}.");
            }
            
            product.UpdatePrice(request.Price);
            await _repository.Save(product);
            return product.Id;
        }
    }
}
