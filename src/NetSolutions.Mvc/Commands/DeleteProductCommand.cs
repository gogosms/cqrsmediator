﻿using System;
using MediatR;

namespace NetSolutions.Mvc.Commands
{
    public class DeleteProductCommand : IRequest<Guid>
    {

        public Guid Id { get; set; }
        
        public DeleteProductCommand(Guid id)
        {
            Id = id;
        }
        
    }
}