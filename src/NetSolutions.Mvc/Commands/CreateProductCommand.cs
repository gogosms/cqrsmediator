﻿using System;
using MediatR;
using Newtonsoft.Json;

namespace NetSolutions.Mvc.Commands
{
    public class CreateProductCommand : IRequest<Guid>
    {
        public string Description { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        [JsonConstructor]
        public CreateProductCommand(string name, string description, double price)
        {
            Name = name;
            Description = description;
            Price = price;
        }
    }
}
