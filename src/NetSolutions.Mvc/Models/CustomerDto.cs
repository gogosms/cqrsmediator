﻿using System;

namespace NetSolutions.Mvc.Models
{
    public class CustomerDto : IContract, IHasName, IHasId<Guid>
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
    }
}