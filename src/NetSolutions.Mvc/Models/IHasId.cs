﻿namespace NetSolutions.Mvc.Models
{
    public interface IHasId<out T>
    {
        T Id { get; }
    }
}