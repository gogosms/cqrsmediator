﻿using System;

namespace NetSolutions.Mvc.Models
{
    public class ProductDto : IContract, IHasName, IHasId<Guid>
    {
        public ProductDto(Guid id, string name, string description, double price, int version)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            Version = version;
        }

        public int Version { get; set; }

        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public double Price { get; set; }
    }
}
