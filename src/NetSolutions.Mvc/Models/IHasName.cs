﻿namespace NetSolutions.Mvc.Models
{
    public interface IHasName
    {
        string Name { get;}
    }
}