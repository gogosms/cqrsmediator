﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NetSolutions.Domain.Products;
using NetSolutions.Mvc.Decorators;

namespace NetSolutions.Mvc.Queries.Handlers
{
    public class GetProductsQueryHandler : IRequestHandler<GetProductsQuery, IContract>
    {
        private readonly IProductRepository _productRepository;
        private readonly IProviderDecorators _providerDecorators;

        public GetProductsQueryHandler(IProductRepository productRepository, IProviderDecorators providerDecorators)
        {
            _productRepository = productRepository;
            _providerDecorators = providerDecorators;
        }

        public async Task<IContract> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            var product = await _productRepository.Get(request.ProductId);
            var decorator = _providerDecorators.Get(product.GetType());
            Debug.Assert(decorator != null);
            return decorator.Decorate(product);
        }
    }
}
