﻿using System;
using MediatR;

namespace NetSolutions.Mvc.Queries
{
    public class GetProductsQuery : IRequest<IContract>
    {
        public Guid ProductId { get; }

        public GetProductsQuery(Guid id)
        {
            ProductId = id;
        }
    }
}
