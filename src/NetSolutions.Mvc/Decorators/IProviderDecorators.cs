﻿using System;

namespace NetSolutions.Mvc.Decorators
{
    public interface IProviderDecorators
    {
        IQueryDecorator Get(Type type);
    }
}