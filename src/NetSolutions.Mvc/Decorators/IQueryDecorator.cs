﻿using System;

namespace NetSolutions.Mvc.Decorators
{
    public interface IQueryDecorator
    {
        bool CanDecorate(Type type);
        
        IContract Decorate(object productObject);
    }
}