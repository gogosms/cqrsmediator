﻿using System;
using NetSolutions.Domain.Products;
using NetSolutions.Mvc.Models;

namespace NetSolutions.Mvc.Decorators
{
    public class ProductQueryDecorator: IQueryDecorator
    {
        public bool CanDecorate(Type type)
        {
            return type == typeof(Product);
        }

        public IContract Decorate(object productObject) 
        {
            if (productObject is Product product)
            {
                return new ProductDto(product.Id, product.Name, product.Description , product.Price, product.Version);
            }

            return null;
        }

    }
}