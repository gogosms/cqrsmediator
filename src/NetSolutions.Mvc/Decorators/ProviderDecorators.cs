﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NetSolutions.Mvc.Decorators
{
    public class ProviderDecorators : IProviderDecorators
    {
        private readonly IEnumerable<IQueryDecorator> _decorators;

        public ProviderDecorators(IEnumerable<IQueryDecorator> decorators)
        {
            _decorators = decorators;
        }

        public IQueryDecorator Get(Type type)
        {
            return _decorators.FirstOrDefault(d => d.CanDecorate(type));
        }
        
    }
}
