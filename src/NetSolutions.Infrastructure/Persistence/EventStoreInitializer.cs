﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NetSolutions.EventSourcing.Abstractions.Domain;
using NetSolutions.EventSourcing.Abstractions.Persistence;

namespace NetSolutions.Infrastructure.Persistence
{
    public class EventStoreInitializer<TEventDbContext, TAggregate, TKey>
        : IEventStoreInitializer<TAggregate, TKey>
        where TEventDbContext : DbContext, IEventDbContext<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        private readonly TEventDbContext _context;

        public EventStoreInitializer(TEventDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task EnsureCreatedAsync(CancellationToken cancellationToken = default)
        {
            return _context.Database.EnsureCreatedAsync(cancellationToken);
        }
    }
}
