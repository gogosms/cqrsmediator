﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NetSolutions.EventSourcing.Abstractions.DependencyInjection;
using NetSolutions.EventSourcing.Abstractions.Domain;
using NetSolutions.EventSourcing.Abstractions.Persistence;
using NetSolutions.EventSourcing.Abstractions.Snapshotting;
using NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence;
using NetSolutions.Infrastructure.Persistence.Snapshotting;

namespace NetSolutions.Infrastructure.Persistence
{
    public static class EventSourcingBuilderExtensions
    {
        public static IEventSourcingBuilder UseEfCoreEventStore<TEventDbContext, TAggregate, TKey>(
            this IEventSourcingBuilder builder)
            where TEventDbContext : DbContext, IEventDbContext<TAggregate, TKey>
            where TAggregate : IAggregate<TKey>
        {
            if (builder is null)
            {
                throw new System.ArgumentNullException(nameof(builder));
            }

            builder.Services
                .AddScoped<IEventStore<TAggregate, TKey>, EventStore<TEventDbContext, TAggregate, TKey>>()
                .AddScoped<IEventStoreInitializer<TAggregate, TKey>, EventStoreInitializer<TEventDbContext, TAggregate, TKey>>()
                ;

            return builder.TryUseDefaultSnapshotStore<TAggregate, TKey>();
        }

        public static IEventSourcingBuilder UseEfCoreSnapshotStore<TSnapshotDbContext, TAggregate, TKey>(
            this IEventSourcingBuilder builder)
            where TSnapshotDbContext : DbContext, ISnapshotDbContext<TAggregate, TKey>
            where TAggregate : IAggregate<TKey>
        {
            if (builder is null)
            {
                throw new System.ArgumentNullException(nameof(builder));
            }

            builder.Services
                .AddScoped<ISnapshotStore<TAggregate, TKey>, SnapshotStore<TSnapshotDbContext, TAggregate, TKey>>()
                .AddScoped<ISnapshotStoreInitializer<TAggregate, TKey>, SnapshotStoreInitializer<TSnapshotDbContext, TAggregate, TKey>>()
                ;

            return builder;
        }
    }
}
