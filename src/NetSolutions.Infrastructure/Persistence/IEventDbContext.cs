﻿using Microsoft.EntityFrameworkCore;
using NetSolutions.EventSourcing.Abstractions.Domain;

namespace NetSolutions.Infrastructure.Persistence
{
    public interface IEventDbContext<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        DbSet<EventEntity<TKey>> GetEventDbSet();
    }
}
