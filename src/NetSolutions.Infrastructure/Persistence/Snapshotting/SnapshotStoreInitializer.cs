﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NetSolutions.EventSourcing.Abstractions.Domain;
using NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence;

namespace NetSolutions.Infrastructure.Persistence.Snapshotting
{
    public class SnapshotStoreInitializer<TSnapshotDbContext, TAggregate, TKey>
        : ISnapshotStoreInitializer<TAggregate, TKey>
        where TSnapshotDbContext : DbContext, ISnapshotDbContext<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        private readonly TSnapshotDbContext _context;

        public SnapshotStoreInitializer(TSnapshotDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task EnsureCreatedAsync(CancellationToken cancellationToken = default)
        {
            return _context.Database.EnsureCreatedAsync(cancellationToken);
        }
    }
}
