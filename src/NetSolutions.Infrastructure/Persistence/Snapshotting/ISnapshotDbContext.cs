﻿using Microsoft.EntityFrameworkCore;
using NetSolutions.EventSourcing.Abstractions.Domain;

namespace NetSolutions.Infrastructure.Persistence.Snapshotting
{
    public interface ISnapshotDbContext<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        DbSet<SnapshotEntity<TKey>> GetSnapshotDbSet();
    }
}
