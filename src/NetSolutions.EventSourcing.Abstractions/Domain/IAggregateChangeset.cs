﻿using System.Collections.Generic;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.EventSourcing.Abstractions.Domain
{
    public interface IAggregateChangeset<TKey>
    {
        IEnumerable<IAggregateEvent<TKey>> Events { get; }

        IAggregateSnapshot<TKey> Snapshot { get; }

        void Commit();
    }
}