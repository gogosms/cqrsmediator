﻿using System.Collections.Generic;
using NetSolutions.EventSourcing.Abstractions.Events;

namespace NetSolutions.EventSourcing.Abstractions.Domain
{
    public interface IAggregate<TKey>
    {
        TKey Id { get; }
        int Version { get; }
        IAggregateSnapshot<TKey> Snapshot { get; }
        IEnumerable<IAggregateEvent<TKey>> Events { get; }
        IAggregateChangeset<TKey> GetChangeset();
    }
}