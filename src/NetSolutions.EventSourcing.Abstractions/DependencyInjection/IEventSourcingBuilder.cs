﻿using Microsoft.Extensions.DependencyInjection;

namespace NetSolutions.EventSourcing.Abstractions.DependencyInjection
{
    public interface IEventSourcingBuilder
    {
        IServiceCollection Services { get; }
    }
}