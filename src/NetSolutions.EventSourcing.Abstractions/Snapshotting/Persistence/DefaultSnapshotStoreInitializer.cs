﻿using System.Threading;
using System.Threading.Tasks;
using NetSolutions.EventSourcing.Abstractions.Domain;

namespace NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence
{
    public class DefaultSnapshotStoreInitializer<TAggregate, TKey>
        : ISnapshotStoreInitializer<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        public Task EnsureCreatedAsync(CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }
    }
}
