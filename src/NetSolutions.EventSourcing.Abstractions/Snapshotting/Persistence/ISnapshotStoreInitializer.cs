﻿using System.Threading;
using System.Threading.Tasks;
using NetSolutions.EventSourcing.Abstractions.Domain;

namespace NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence
{
    public interface ISnapshotStoreInitializer<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        Task EnsureCreatedAsync(CancellationToken cancellationToken = default);
    }
}
