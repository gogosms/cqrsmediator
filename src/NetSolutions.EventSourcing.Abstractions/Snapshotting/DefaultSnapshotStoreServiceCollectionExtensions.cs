﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using NetSolutions.EventSourcing.Abstractions.DependencyInjection;
using NetSolutions.EventSourcing.Abstractions.Domain;
using NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence;

namespace NetSolutions.EventSourcing.Abstractions.Snapshotting
{
    public static class DefaultSnapshotStoreServiceCollectionExtensions
    {
        public static IEventSourcingBuilder TryUseDefaultSnapshotStore<TAggregate, TKey>(this IEventSourcingBuilder builder)
            where TAggregate: IAggregate<TKey>
        {
            if (builder is null)
            {
                throw new System.ArgumentNullException(nameof(builder));
            }

            builder.Services
                .TryAddScoped<ISnapshotStore<TAggregate, TKey>, DefaultSnapshotStore<TAggregate, TKey>>();

            builder.Services
                .TryAddScoped<ISnapshotStoreInitializer<TAggregate, TKey>, DefaultSnapshotStoreInitializer<TAggregate, TKey>>();

            return builder;

        }
    }
}
