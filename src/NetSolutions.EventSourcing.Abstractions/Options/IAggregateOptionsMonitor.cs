﻿using NetSolutions.EventSourcing.Abstractions.Domain;

namespace NetSolutions.EventSourcing.Abstractions.Options
{
    public interface IAggregateOptionsMonitor<TAggregate, TKey, out TOptions>
        where TAggregate: IAggregate<TKey>
        where TOptions: class
    {
        TOptions AggregateOptions { get; }
    }
}
