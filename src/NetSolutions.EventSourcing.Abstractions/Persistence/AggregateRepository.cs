﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NetSolutions.EventSourcing.Abstractions.Domain;
using NetSolutions.EventSourcing.Abstractions.Events;
using NetSolutions.EventSourcing.Abstractions.Snapshotting.Persistence;

namespace NetSolutions.EventSourcing.Abstractions.Persistence
{
    public abstract class AggregateRepository<TAggregate, TKey>
        where TAggregate : class, IAggregate<TKey>
    {
        private readonly IEventStore<TAggregate, TKey> _eventStore;
        private readonly ISnapshotStore<TAggregate, TKey> _snapshotStore;

        protected AggregateRepository(
            IEventStore<TAggregate, TKey> eventStore,
            ISnapshotStore<TAggregate, TKey> snapshotStore)
        {
            _eventStore = eventStore ?? throw new ArgumentNullException(nameof(eventStore));
            _snapshotStore = snapshotStore ?? throw new ArgumentNullException(nameof(snapshotStore));
        }

        protected virtual async Task<IAggregateChangeset<TKey>> SaveAggregateAsync(TAggregate aggregate,
            CancellationToken cancellationToken = default)
        {
            if (aggregate is null)
            {
                throw new ArgumentNullException(nameof(aggregate));
            }

            var aggregateChangeSet = aggregate.GetChangeset();
            foreach (var @event in aggregateChangeSet.Events)
            {
                await _eventStore.AddEventAsync(@event, cancellationToken).ConfigureAwait(false);
            }

            if (aggregateChangeSet.Snapshot != null)
            {
                await _snapshotStore.AddSnapshotAsync(aggregateChangeSet.Snapshot, cancellationToken).ConfigureAwait(false);
            }

            aggregateChangeSet.Commit();
            return aggregateChangeSet;
        }

        protected virtual Task<TKey[]> GetAggregateIdsAsync()
        {
            return _eventStore.GetAggregateIdsAsync();
        }

        protected virtual async Task<TAggregate> FindAggregateAsync(TKey id,
            bool ignoreSnapshot = false,
            int version = -1,
            CancellationToken cancellationToken = default)
        {
            var maxVersion = version <= 0 ? int.MaxValue : version;

            IAggregateSnapshot<TKey> snapshot = null;
            if (!ignoreSnapshot)
            {
                snapshot = await _snapshotStore
                    .FindLastSnapshotAsync(id, maxVersion,  cancellationToken)
                    .ConfigureAwait(false);
            }

            var minVersion = snapshot?.AggregateVersion + 1 ?? 1;
            var events = await _eventStore
                .GetEventsAsync(id, minVersion, maxVersion, cancellationToken)
                .ConfigureAwait(false);

            if (snapshot == null)
            {
                return events.Length == 0
                    ? null
                    : Activator.CreateInstance(typeof(TAggregate), id, (IEnumerable<IAggregateEvent<TKey>>) events) as TAggregate;
            }
            else
            {
                return Activator.CreateInstance(typeof(TAggregate), id, snapshot, (IEnumerable<IAggregateEvent<TKey>>) events) as TAggregate;
            }
        }
    }
}
