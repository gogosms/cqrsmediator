﻿using System.Threading;
using System.Threading.Tasks;
using NetSolutions.EventSourcing.Abstractions.Domain;

namespace NetSolutions.EventSourcing.Abstractions.Persistence
{
    public interface IEventStoreInitializer<TAggregate, TKey>
        where TAggregate : IAggregate<TKey>
    {
        Task EnsureCreatedAsync(CancellationToken cancellationToken = default);
    }
}
