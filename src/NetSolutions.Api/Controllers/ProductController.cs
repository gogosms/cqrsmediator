﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NetSolutions.Mvc.Commands;
using NetSolutions.Mvc.Queries;

namespace NetSolutions.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
            
        }

        [Route("{id:Guid}")]
        [HttpGet]
        public async Task<IActionResult> GetProduct(Guid id)
        {
            try
            {
                var product = await _mediator.Send(new GetProductsQuery(id));
                return Ok(product);
            }
            catch
            {
                return NotFound();
            }
        }

        [Route("")]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateProductCommand command)
        {
            var productId = await _mediator.Send(command).ConfigureAwait(false);
            return Ok(productId);
        }

        [Route("")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateProductCommand command)
        {
            var productId = await _mediator.Send(command).ConfigureAwait(false);
            return Ok(productId);
     
        }

        [Route("{id:Guid}")]
        [HttpDelete]
        public async Task<IActionResult> Create(Guid id)
        {
            var productId = await _mediator.Send(new DeleteProductCommand(id)).ConfigureAwait(false);
            return Created(string.Empty, productId);

        }
    }
}