using System;
using System.Diagnostics;
using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetSolutions.Api.Database;
using NetSolutions.Domain.Products;
using NetSolutions.Infrastructure.Persistence;
using NetSolutions.Mvc;
using NetSolutions.Mvc.Decorators;
using NetSolutions.Mvc.Queries.Handlers;

namespace NetSolutions.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(GetProductsQueryHandler).GetTypeInfo().Assembly);
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddSingleton<IQueryDecorator, ProductQueryDecorator>();
            services.AddSingleton<IProviderDecorators, ProviderDecorators>();
            services.AddControllers().AddNewtonsoftJson();
            ConfigureServicesForEfCore(services);
        }

        private void ConfigureServicesForEfCore(IServiceCollection services)
        {
            services.AddDbContext<EfDbContext>(options =>
            {
                var connectionString = Configuration.GetConnectionString("ProductDb");
                Debug.Assert(!string.IsNullOrEmpty(connectionString));
                options.UseSqlServer(connectionString);
            });
            
            services
                .AddEventSourcing(builder =>
                {
                    builder
                        .UseEfCoreEventStore<EfDbContext, Product, Guid>()
                        .UseEfCoreSnapshotStore<EfDbContext, Product, Guid>();
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
