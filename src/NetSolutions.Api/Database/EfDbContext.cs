﻿using System;
using Microsoft.EntityFrameworkCore;
using NetSolutions.Domain.Products;
using NetSolutions.Infrastructure.Persistence;
using NetSolutions.Infrastructure.Persistence.Snapshotting;

namespace NetSolutions.Api.Database
{
    /// <summary>
    /// Entity Framework Context
    /// </summary>
    public class EfDbContext : DbContext,
        IEventDbContext<Product, Guid>,
        ISnapshotDbContext<Product, Guid>
    {

        public EfDbContext(DbContextOptions<EfDbContext> options)
            : base(options)
        { }

        public DbSet<EventEntity<Guid>> ProductEvents { get; set; }

        public DbSet<SnapshotEntity<Guid>> ProductSnapshots { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EventEntityConfiguration<Guid>());
            modelBuilder.ApplyConfiguration(new SnapshotEntityConfiguration<Guid>());
        }

        DbSet<EventEntity<Guid>> IEventDbContext<Product, Guid>.GetEventDbSet() => ProductEvents;
        DbSet<SnapshotEntity<Guid>> ISnapshotDbContext<Product, Guid>.GetSnapshotDbSet() => ProductSnapshots;
    }
}
