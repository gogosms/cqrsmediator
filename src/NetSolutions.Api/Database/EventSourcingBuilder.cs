using Microsoft.Extensions.DependencyInjection;
using NetSolutions.EventSourcing.Abstractions.DependencyInjection;

namespace NetSolutions.Api.Database
{
    public class EventSourcingBuilder : IEventSourcingBuilder
    {
        public EventSourcingBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }
    }
}