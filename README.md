# README #
Project CQRS and Mediator Patters

Notice how the CQRS diagram has two endpoints servicing the UI, not just one? 
The Mediator pattern defines an object that encapsulates how a set of objects interact. 
Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.
